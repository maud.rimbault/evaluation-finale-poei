package com.zenika.academy.evalfinalepoei.web.users;

public record UserDto (String username, String email, String password) {
}
