package com.zenika.academy.evalfinalepoei.domain.model.answer;

import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import com.zenika.academy.evalfinalepoei.domain.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "answers")
@Access(AccessType.FIELD)
public class Answer {
    @Id
    @Column(name= "answerid")
    private String answerId;
    private String content;
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userid")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "questionid")
    private Question question;

    protected Answer(){
        //FOR JPA
    }

    public Answer(String answerId, String content, LocalDateTime date, User user, Question question) {
        this.answerId = answerId;
        this.content = content;
        this.date = date;
        this.question = question;
        this.user = user;
    }

    public String getAnswerId() {
        return answerId;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Question getQuestion() {
        return question;
    }

    public User getUser() {
        return user;
    }
}
