package com.zenika.academy.evalfinalepoei.web.answers;

import com.zenika.academy.evalfinalepoei.application.AnswerService;
import com.zenika.academy.evalfinalepoei.domain.model.answer.Answer;
import com.zenika.academy.evalfinalepoei.domain.model.answer.BadLengthContentException;
import com.zenika.academy.evalfinalepoei.domain.model.question.QuestionNotFoundException;
import com.zenika.academy.evalfinalepoei.domain.model.user.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/answers")
public class AnswerController {
    private final AnswerService answerService;

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    //Creation answer
    @PostMapping
    ResponseEntity<Answer> createAnswer(@RequestBody AnswerDto body, @RequestParam String questionId) throws BadLengthContentException, QuestionNotFoundException, UserNotFoundException {
        Answer createdAnswer = answerService.createAnswer(body.content(), questionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAnswer);
    }

    //Afficher toutes les réponses
//    @GetMapping
//    @ResponseStatus(value = HttpStatus.OK)
//    Iterable<Answer> listAnswers() {
//        return this.answerService.getAllAnswers();
//    }

    //Afficher toutes les réponses ou les réponses d'une question
    @GetMapping
    ResponseEntity<Iterable<Answer>> listAnswers(@RequestParam(required = false) String questionId) {
        if (questionId != null){
            return new ResponseEntity<>(this.answerService.getAllAnswersForOneQuestion(questionId), HttpStatus.OK);
        }
        return new ResponseEntity<>(this.answerService.getAllAnswers(), HttpStatus.OK);
    }

//    @ResponseStatus(value = HttpStatus.OK)
//    Optional<Answer> listAnswersForOneQuestion(@RequestParam(required = false) String questionId) {
//        return this.answerService.getAllAnswersForOneQuestion(questionId);
//    }


    //Afficher une réponse
    @GetMapping("/{answerId}")
    public ResponseEntity<Answer> getOneAnswerById(@PathVariable("answerId") String answerId){
        Optional<Answer> foundAnswer = this.answerService.getAnswer(answerId);
        return foundAnswer.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    //Delete une réponse
    @DeleteMapping("/{answerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteAnswer(@PathVariable String answerId) {
        this.answerService.deleteAnswer(answerId);
    }
}
