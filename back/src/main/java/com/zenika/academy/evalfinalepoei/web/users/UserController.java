package com.zenika.academy.evalfinalepoei.web.users;

import com.zenika.academy.evalfinalepoei.application.UserService;
import com.zenika.academy.evalfinalepoei.domain.model.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

//    @PostMapping("/me")
//    @ResponseStatus(value = HttpStatus.OK)
//    public String me() {
//        return SecurityContextHolder.getContext().getAuthentication().getName();
//    }


    @PostMapping("/signup")
    ResponseEntity<User> createUser(@RequestBody UserDto body) {
        User createdUser = this.userService.createUser(body.username(), body.email(), body.password());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

}
