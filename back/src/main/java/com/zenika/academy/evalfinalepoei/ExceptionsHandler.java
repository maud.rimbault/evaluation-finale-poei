package com.zenika.academy.evalfinalepoei;

import com.zenika.academy.evalfinalepoei.domain.model.answer.BadLengthContentException;
import com.zenika.academy.evalfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.academy.evalfinalepoei.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(value = BadLengthContentException.class)
    public final ResponseEntity<ApiError> BadLengthContentException(BadLengthContentException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le contenu de la réponse ne doit pas dépasser 100 mots"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadLengthTitleException.class)
    public final ResponseEntity<ApiError> BadLengthTitleException(BadLengthTitleException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre ne doit pas dépasser 20 mots et finir par un point d'interrogation"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public final ResponseEntity<ApiError> UserNotFoundException(UserNotFoundException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "L'utilisateur n'existe pas"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public final ResponseEntity<ApiError> UsernameNotFoundException(UsernameNotFoundException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Ce nom d'utilisateur n'existe pas"), HttpStatus.BAD_REQUEST);
    }

}
