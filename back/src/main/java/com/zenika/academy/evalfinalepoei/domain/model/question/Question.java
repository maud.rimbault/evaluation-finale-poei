package com.zenika.academy.evalfinalepoei.domain.model.question;

import com.zenika.academy.evalfinalepoei.domain.model.user.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "questions")
@Access(AccessType.FIELD)
public class Question {
    @Id
    @Column(name="questionid")
    private String questionId;
    private String title;
    private String content;
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userid")
    private User user;

    protected Question(){
        // FOR JPA
    }


    public Question(String questionId, String title, String content, LocalDateTime date, User user) {
        this.questionId = questionId;
        this.title = title;
        this.content = content;
        this.date = date;
        this.user = user;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public User getUser() {
        return user;
    }
}
