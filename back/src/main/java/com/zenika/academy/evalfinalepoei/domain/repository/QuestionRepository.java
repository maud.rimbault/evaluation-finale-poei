package com.zenika.academy.evalfinalepoei.domain.repository;

import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository <Question, String>{
    @Query(value = "select * from questions where word_similarity(?1, questions.title) > 0.3", nativeQuery = true)
    Iterable<Question> findAllByTitleContaining(@Param("title") String title);

    Iterable<Question> findAll(Sort date);
}
