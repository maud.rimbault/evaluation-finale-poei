package com.zenika.academy.evalfinalepoei.web.questions;

import com.zenika.academy.evalfinalepoei.application.QuestionService;
import com.zenika.academy.evalfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import com.zenika.academy.evalfinalepoei.domain.model.user.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/questions")
public class QuestionController {
    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    //Creation question
    @PostMapping
    ResponseEntity<Question> createQuestion(@RequestBody QuestionDto body) throws BadLengthTitleException, UserNotFoundException {
        Question createdQuestion = questionService.createQuestion(body.title(), body.content());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdQuestion);
    }

//    //Afficher toutes les questions
//    @GetMapping
//    @ResponseStatus(value = HttpStatus.OK)
//    Iterable<Question> listQuestions() {
//        return this.questionService.getAllQuestions();
//    }

    //Afficher toutes les questions ou findByTitle
    @GetMapping
    ResponseEntity<Iterable<Question>> listQuestions(@RequestParam(required = false) String wordTitle) {
        if (wordTitle != null){
            return new ResponseEntity<>(this.questionService.getQuestionsFindByTitle(wordTitle), HttpStatus.OK);
        }
        return new ResponseEntity<>(this.questionService.getAllQuestions(), HttpStatus.OK);
    }

    //Afficher une question
    @GetMapping("/{questionId}")
    public ResponseEntity<Question> getOneQuestionById(@PathVariable("questionId") String questionId){
        Optional <Question> foundQuestion = this.questionService.getQuestion(questionId);
        return foundQuestion.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    //Delete une question
    @DeleteMapping("/{questionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteQuestion(@PathVariable String questionId) {
        this.questionService.deleteQuestion(questionId);
    }
}
