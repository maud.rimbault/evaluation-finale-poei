package com.zenika.academy.evalfinalepoei.application;

import com.zenika.academy.evalfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import com.zenika.academy.evalfinalepoei.domain.model.user.User;
import com.zenika.academy.evalfinalepoei.domain.model.user.UserNotFoundException;
import com.zenika.academy.evalfinalepoei.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class QuestionService {
    private final QuestionRepository questionRepository;
    private final UserService userService;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, UserService userService) {
        this.questionRepository = questionRepository;
        this.userService = userService;
    }

    public Question createQuestion(String title, String content) throws BadLengthTitleException, UserNotFoundException {

        String [] words = title.split(" ");
        int numberOfWords = words.length;
        System.out.println(numberOfWords);
        if (numberOfWords <= 20 && title.endsWith("?")) {
            Optional<User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);

            Question question = new Question(UUID.randomUUID().toString(), title, content, LocalDateTime.now().withNano(0), user);
            questionRepository.save(question);
            return question;
        } else {
            throw new BadLengthTitleException();
        }
    }



//    public Question createQuestion(String title, String content, String usernameAuthor) throws BadLengthTitleException, UserNotFoundException {
//
//        String [] words = title.split(" ");
//        int numberOfWords = words.length;
//        System.out.println(numberOfWords);
//        if (numberOfWords <= 20) {
//            User user = usernameAuthor != null
//                    ? userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new)
//                    : null;
//
////            User user = usernameAuthor != null
////                    ? userService.getUserByUsername(usernameAuthor).orElseThrow(UserNotFoundException::new)
////                    : null;
//
//            Question question = new Question(UUID.randomUUID().toString(), title, content, LocalDateTime.now().withNano(0), user);
//            questionRepository.save(question);
//            return question;
//        } else {
//            throw new BadLengthTitleException();
//        }
//    }

    public Iterable<Question> getAllQuestions(){
        return questionRepository.findAll();
    }

    public Optional<Question> getQuestion(String questionId){
        return questionRepository.findById(questionId);
    }

    public Iterable<Question> getQuestionsFindByTitle(String wordTitle){
        return questionRepository.findAllByTitleContaining(wordTitle);
    }

    public void deleteQuestion(String id) {
        this.questionRepository.deleteById(id);
    }
}
