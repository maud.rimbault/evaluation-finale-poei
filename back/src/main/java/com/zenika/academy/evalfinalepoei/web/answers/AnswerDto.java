package com.zenika.academy.evalfinalepoei.web.answers;

public record AnswerDto (String content, String usernameAuthor) {
}
