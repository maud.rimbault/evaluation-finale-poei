package com.zenika.academy.evalfinalepoei.web.questions;

public record QuestionDto (String title, String content, String usernameAuthor){
}
