package com.zenika.academy.evalfinalepoei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvalfinalepoeiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvalfinalepoeiApplication.class, args);
	}

}
