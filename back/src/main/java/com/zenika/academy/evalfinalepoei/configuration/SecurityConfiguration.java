package com.zenika.academy.evalfinalepoei.configuration;

import com.zenika.academy.evalfinalepoei.domain.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SecurityConfiguration {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                //.authorizeRequests(authorize -> authorize
                //.authorizeRequests().antMatchers(HttpMethod.GET, "/api/articles").permitAll()
                //.anyRequest().authenticated()
                //.and()
                .authorizeRequests(authorize -> authorize
                        .antMatchers(HttpMethod.GET, "/questions").permitAll()
                        .antMatchers(HttpMethod.GET, "/questions/*").permitAll() //autorise 2 routes GET
                        .antMatchers(HttpMethod.GET, "/answers").permitAll()
                        .antMatchers(HttpMethod.GET, "/answers/*").permitAll()
                        .antMatchers(HttpMethod.GET, "/users/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/users/signup").permitAll()
                        .anyRequest().authenticated())
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

//    @Bean
//    public UserDetailsService users() {
//        UserDetails maud = User.builder()
//                .username("maud")
//                //.password("{noop}maud")
//                .password("{bcrypt}$2y$10$qMd0X74BW87k9QSpddT9X.XXcFXe91JPt2QX9l0Ar8wnjcmhkGHMe")
//                .roles("USER")
//                .build();
////        UserDetails maxime = User.builder()
////                .username("maxime")
////                .password("{bcrypt}$2a$10$cq3mD7wNj7zviWJTZA4ZauuAaqhBjSvdpOh9AsrS
////                        MuoXD88.Sifsa")
////                                .roles("USER")
////                                .build();
//        UserDetails amandine = User.builder()
//                .username("amandine")
//                .password("{bcrypt}$2y$10$7Xqo4PuG53IMH1yJ8fKCy.0b6Hy0H56SZC01NYxTE8UhIIGSiTFOG")
//                .roles("USER")
//                .build();
//        return new InMemoryUserDetailsManager(maud, amandine);
//    }

    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return userRepository.findByUsername(username).orElse(null);
            }
        };
    }

    //encoder le password à l'inscription
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
