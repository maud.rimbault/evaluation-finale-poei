package com.zenika.academy.evalfinalepoei.domain.repository;

import com.zenika.academy.evalfinalepoei.domain.model.answer.Answer;
import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends CrudRepository <Answer, String> {
    Iterable<Answer> findAllByQuestion(Question question);
}