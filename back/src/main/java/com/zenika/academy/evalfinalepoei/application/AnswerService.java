package com.zenika.academy.evalfinalepoei.application;

import com.zenika.academy.evalfinalepoei.domain.model.answer.Answer;
import com.zenika.academy.evalfinalepoei.domain.model.answer.BadLengthContentException;
import com.zenika.academy.evalfinalepoei.domain.model.question.Question;
import com.zenika.academy.evalfinalepoei.domain.model.question.QuestionNotFoundException;
import com.zenika.academy.evalfinalepoei.domain.model.user.User;
import com.zenika.academy.evalfinalepoei.domain.model.user.UserNotFoundException;
import com.zenika.academy.evalfinalepoei.domain.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionService questionService;
    private final UserService userService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository, QuestionService questionService, UserService userService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.userService = userService;
    }

    public Answer createAnswer(String content, String questionId) throws BadLengthContentException, QuestionNotFoundException, UserNotFoundException {
        Question question = questionId != null
                ?
                questionService.getQuestion(questionId).orElseThrow(QuestionNotFoundException::new)
                : null;

        String [] words = content.split(" ");
        int numberOfWords = words.length;
        System.out.println(numberOfWords);
        if (numberOfWords <= 100) {
            Optional<User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);

            Answer answer = new Answer(UUID.randomUUID().toString(), content, LocalDateTime.now().withNano(0), user, question);
            answerRepository.save(answer);
            return answer;
        } else {
            throw new BadLengthContentException();
        }
    }

//    public Answer createAnswer(String content, String usernameAuthor, String questionId) throws BadLengthContentException, QuestionNotFoundException, UserNotFoundException {
//        Question question = questionId != null
//                ?
//                questionService.getQuestion(questionId).orElseThrow(QuestionNotFoundException::new)
//                : null;
//
//        User user = usernameAuthor != null
//                ? userService.getUserByUsername(usernameAuthor).orElseThrow(UserNotFoundException::new)
//                : null;
//
//        String [] words = content.split(" ");
//        int numberOfWords = words.length;
//        System.out.println(numberOfWords);
//        if (numberOfWords <= 100) {
//            Answer answer = new Answer(UUID.randomUUID().toString(), content, LocalDateTime.now().withNano(0), user, question);
//            answerRepository.save(answer);
//            return answer;
//        } else {
//            throw new BadLengthContentException();
//        }
//    }




    public Iterable<Answer> getAllAnswers(){
        return answerRepository.findAll();
    }


    public Iterable<Answer> getAllAnswersForOneQuestion(String questionId){
        return answerRepository.findAllByQuestion(questionService.getQuestion(questionId).get());
    }


    public Optional<Answer> getAnswer(String answerId){
        return answerRepository.findById(answerId);
    }

    public void deleteAnswer(String id) {
        this.answerRepository.deleteById(id);
    }
}
