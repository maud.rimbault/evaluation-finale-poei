package com.zenika.academy.evalfinalepoei.domain.repository;

import com.zenika.academy.evalfinalepoei.domain.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository <User, String>{
    public Optional<User> findByUsername(String username);
}
