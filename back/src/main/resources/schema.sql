CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS users(
   userid char(36) primary key,
   username text not null unique,
   email text not null unique,
   password text not null
);

CREATE TABLE IF NOT EXISTS questions(
   questionid char(36) primary key,
   title text not null unique,
   content text not null,
   date TIMESTAMP not null,
   userid char(36) REFERENCES users(userid)
);

CREATE TABLE IF NOT EXISTS answers(
   answerid char(36) primary key,
   content text not null,
   date TIMESTAMP not null,
   questionid char(36) references questions(questionid),
   userid char(36) REFERENCES users(userid)
);