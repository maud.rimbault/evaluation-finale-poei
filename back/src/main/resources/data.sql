INSERT INTO users(userid, username, email,  password)
VALUES('e3e65cb2-c0ae-11ec-9d64-0242ac120002', 'maud', 'maud@test.fr', '$2y$10$1zT5akmsifx5hYYkTggDt.UEQXtS5ha7JmJ3qMpbooIoPl13vE4hu') ON CONFLICT DO NOTHING;

INSERT INTO users(userid, username, email, password)
VALUES('407144a3-1e81-4e0e-bc85-7e8302363f15', 'camille', 'camille@test.fr', '$2y$10$X985lsMkZp42iYiymFcaH.ra90qGCcJeVn754mKTc8bD83ylkXqrW') ON CONFLICT DO NOTHING;

INSERT INTO users(userid, username, email, password)
VALUES('e77c4246-9d6e-4c3c-b24b-86841ac6c9de', 'tom', 'tom@test.fr', '$2y$10$LsyAL/3XehbguEUPikTWsuO06Kcq1jasaI7YaQDTP8SOeeHCX2GUi') ON CONFLICT DO NOTHING;

INSERT INTO questions(questionid, title, content, date, userid)
VALUES('49e2508a-e16e-459a-ae36-2e7fdd4b4803', 'What is the difference between PUT, POST and PATCH ?', 'What is the difference between PUT, POST and PATCH methods in HTTP protocol?', '2022-05-04T09:54:17', 'e3e65cb2-c0ae-11ec-9d64-0242ac120002') ON CONFLICT DO NOTHING;

INSERT INTO answers(answerid, content, questionid, date, userid)
VALUES('8338ae87-a4cf-4678-b408-92f2ae549268'
, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempus mauris id sem tincidunt consectetur. Proin congue elit commodo massa lobortis sodales. In hac habitasse platea dictumst. Fusce consequat erat in rutrum interdum. Duis nibh lorem, aliquam at ornare ac, posuere sit amet nulla. Proin sollicitudin cursus sapien, nec cursus.'
, '49e2508a-e16e-459a-ae36-2e7fdd4b4803'
, '2022-05-04T10:54:17'
, 'e3e65cb2-c0ae-11ec-9d64-0242ac120002') ON CONFLICT DO NOTHING;

INSERT INTO questions(questionid, title, content, date, userid)
VALUES('6148a787-e454-4389-8d1c-6a0f3bec4904', 'How do I undo the most recent local commits in Git?', 'I accidentally committed the wrong files to Git but didn t push the commit to the server yet. How can I undo those commits from the local repository?', '2022-05-04T08:54:17', 'e77c4246-9d6e-4c3c-b24b-86841ac6c9de') ON CONFLICT DO NOTHING;
