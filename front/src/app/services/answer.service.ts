import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../models/answer.model';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {
  

  // public async fetchAnswers() {
  //   await this.getListAnswers();
  // }

  public async getListAnswers(questionId: string): Promise<Answer[]> {    
    return await axios.get(`http://localhost:8080/api/answers?questionId=${questionId}`)
      .then((res) => {
        //console.log(res.data);
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        return null;
      });
 }

 public createAnswer(answerData: Answer, questionId: string) {
  const apiUrl = "http://localhost:8080/api/answers/";
  return axios.post(`${apiUrl}`, answerData, { params: {questionId}})
    // .then((res) => {
    //   console.log(res.data);
    //   this.getListQuestions();
    // })
    // .catch((err) => {
    //   console.log(err);
    // });
}



  constructor() { }
}
