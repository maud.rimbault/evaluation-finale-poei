import { Injectable } from '@angular/core';
import axios from 'axios';
import { Question } from '../models/question.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  public questions: Question[] = [];

  public message: string = '';

  public async fetchQuestions() {
    await this.getListQuestions();
  }

  public async getListQuestions() {    
    await axios.get("http://localhost:8080/api/questions/")
     .then((res) => {
       //console.log(res.data);
       this.questions = res.data;
     })
     .catch((err) => {
       console.log(err);
     });
     this.questions.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
 }

  public createQuestion(questionData: Question) {
    const apiUrl = "http://localhost:8080/api/questions/";
    return axios.post(`${apiUrl}`, questionData)
      // .then((res) => {
      //   console.log(res.data);
      //   this.getListQuestions();
      // })
      // .catch((err) => {
      //   console.log(err);
      //   this.message = "test";
      // });
  }

  public signup(userData: User) {
    const apiUrl = "http://localhost:8080/api/users/signup";
    return axios.post(`${apiUrl}`, userData)
  }
  
  public getSearchListQuestions(wordTitle: string) {
    console.log(`${wordTitle}`);
   axios.get(`http://localhost:8080/api/questions?wordTitle=${wordTitle}`)
       .then((res) => {
        console.log(res.data);
         this.questions = res.data;
       })
       .catch((err) => {
         console.log(err);
       });
   }
  

  constructor() { }
}
