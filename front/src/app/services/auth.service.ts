import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly SESSION_STORAGE_KEY = "currentUser";
  public isLogged: boolean = false;

  apiUrl: String = 'http://localhost:8080/users';
  public user?: User;

  // verify(): Promise<any> {
  //   return axios.post(`${this.apiUrl}/me`);
  //   }

  login(user: User) {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    sessionStorage.setItem('isLogged', 'true');
    this.isLogged = true;
  }

  logout() {
    sessionStorage.clear();
    sessionStorage.setItem('isLogged', 'false');
    this.isLogged = false;
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return "";
    }
  }

  // getUsername(){
  //   const json = sessionStorage.getItem("currentUser");
  //   if (json) {
  //     return JSON.parse(json).username;
  //     //console.log(currentUsername);
  //   }
  // }

  getCurrentUser(): any {
    const currentUserPlain = sessionStorage.getItem('session')
    //const user?:User;
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return {username : currentUser.pseudo,
        password : currentUser.password};
  }
}

  constructor() { }
}
