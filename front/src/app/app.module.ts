import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { FormQuestionPageComponent } from './pages/form-question-page/form-question-page.component';
import { QuestionComponent } from './components/question/question.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { NavigationComponent } from './components/layout/navigation/navigation.component';
import { SearchComponent } from './components/search/search.component';
import { LucideAngularModule, Plus, Trash2, Search, Home, Edit} from 'lucide-angular';
import { AnswerComponent } from './components/answer/answer.component';
import { FormQuestionComponent } from './components/form-question/form-question.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DetailsPageComponent,
    FormQuestionPageComponent,
    QuestionComponent,
    HeaderComponent,
    NavigationComponent,
    SearchComponent,
    AnswerComponent,
    FormQuestionComponent,
    LoginComponent,
    SignupComponent,
    LoginPageComponent,
    SignupPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LucideAngularModule.pick({Plus, Trash2, Search, Home, Edit})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
