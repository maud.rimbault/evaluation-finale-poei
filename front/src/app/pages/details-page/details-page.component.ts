import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Answer } from 'src/app/models/answer.model';
import { Question } from 'src/app/models/question.model';
import { User } from 'src/app/models/user.model';
import { AnswerService } from 'src/app/services/answer.service';
import { AuthService } from 'src/app/services/auth.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {

  public answers: Answer[] = [];

  private questionId?: string;
  @Input()
  public question?: Question;


  @Output('questionDetails')
  public questionDetailEvent = new EventEmitter<Question>();


  answerId: string = "";
  content: string = "";
  date: string = "";
  //questionId: string="";


  constructor(
    private activatedRoute: ActivatedRoute,
    private questionService: QuestionService,
    public answerService: AnswerService,
    private router: Router,
    private authService: AuthService,
  ) {
    this.questionId = this.activatedRoute.snapshot.params['questionId'];
    console.log(this.activatedRoute.snapshot);
    console.log(this.questionId);

    const question = this.questionService.questions.find((q) => q.questionId === this.questionId);
    if (!question) {
      return;
      ;
    }

    this.question = question;
  }



  async answerFormInfo() {
    const answerData: Answer = {
      answerId: this.answerId,
      content: this.content,
      questionId: this.questionId!,
      date: this.date,
      user: this.authService.getCurrentUser()
      //username: this.authService.getUsername()
    };

    const numberOfWords = this.content.split(" ").length;
    console.log(numberOfWords);
    if (numberOfWords <= 100) {
      this.answerService.createAnswer(answerData, this.questionId!).then(() => {
        // refresh 
        this.answerService.getListAnswers(this.questionId!)
        console.log(this.answerService.getListAnswers(this.questionId!))
        // and go to question
        this.showAnswers();
        //this.router.navigate(['/:questionId']);
        //console.log(this.router.navigate(['/:questionId']))
      }
      )
    } else {
      console.log("erreur");
      window.alert("La réponse ne doit pas dépasser 100 mots")
    }

  }

  async showAnswers() {
    if (this.questionId) {
      this.answers = await this.answerService.getListAnswers(this.questionId);
    }
  }

  async ngOnInit() {
    this.showAnswers();
  }

}
