import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormQuestionPageComponent } from './form-question-page.component';

describe('FormQuestionPageComponent', () => {
  let component: FormQuestionPageComponent;
  let fixture: ComponentFixture<FormQuestionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormQuestionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormQuestionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
