import { User } from "./user.model";

export interface Question {
    questionId: string;
    title: string;
    content: string;
    date: string;
    user: User;
}
