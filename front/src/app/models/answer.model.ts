import { User } from "./user.model";

export interface Answer {
    answerId: string;
    content: string;
    date: string;
    questionId: string;
    user: User;
}
