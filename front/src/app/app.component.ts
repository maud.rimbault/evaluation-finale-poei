import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { authInterceptor, errorInterceptor } from './helpers/axios-interceptor';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'front';

  constructor(public authService: AuthService, private router: Router){
    authInterceptor(this.authService)
    errorInterceptor(this.authService, this.router)
  }

  ngOnInit() {
    if (sessionStorage.getItem('isLogged') === 'true') {
      this.authService.isLogged = true;
    }
  }
}
