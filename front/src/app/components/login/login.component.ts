import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input()
  password: string = "";
  @Input()
  username: string = "";

  constructor(private authService: AuthService, private router: Router) { }

  async loginForm() {
    // const userData:User= {
    //   username: this.username,
    //   password: this.password
    // };

    await this.authService.login({username: this.username, password: this.password});
    //this.authService.verify()
    //.then(() => this.router.navigate(['/']))
    //.catch(() => this.authService.logout())
    console.log("login ok");
    //await this.authService.login(userData);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
