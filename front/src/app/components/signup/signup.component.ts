import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  password: string = "";
  email: string = "";
  username: string = "";

  constructor(public questionService: QuestionService, private router:Router) { }

  async signupForm() {
    const userData:User= {
      username: this.username,
      email: this.email,
      password: this.password
    };

    await this.questionService.signup(userData);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
