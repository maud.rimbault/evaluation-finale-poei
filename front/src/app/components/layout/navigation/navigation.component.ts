import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(public question:QuestionService, public authService: AuthService, public router: Router) { }

  logout(){
    this.authService.logout();
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
