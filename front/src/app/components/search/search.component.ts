import { Component, OnInit } from '@angular/core';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  public wordTitle: string="";

  constructor(private questionService: QuestionService) { }

  getSearchResult(wordTitle: any) {
    this.wordTitle = wordTitle.target.value;
    console.log("mot a trouver : ", this.wordTitle);
    }

  async findWordinTitle(wordTitle: string) {
    await this.questionService.getSearchListQuestions(wordTitle);
  };

}
