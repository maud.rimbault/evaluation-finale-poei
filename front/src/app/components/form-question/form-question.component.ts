import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Question } from 'src/app/models/question.model';
import { AuthService } from 'src/app/services/auth.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-form-question',
  templateUrl: './form-question.component.html',
  styleUrls: ['./form-question.component.scss']
})
export class FormQuestionComponent implements OnInit {
  questionId: string="";
  title: string="";
  content: string="";
  date: string="";
  

  constructor(public questionService: QuestionService, private router:Router, public authService:AuthService) { }

  async questionFormInfo() {

    
      const questionData: Question = {
        title: this.title,
        content: this.content,
        questionId: this.questionId,
        date: this.date,
        user: this.authService.getCurrentUser()
      };
      
      const numberOfWords = this.title.split(" ").length;
      console.log(numberOfWords);
      const lastChar = this.title.endsWith("?");
      console.log(lastChar);
      if (numberOfWords <= 20 && this.title.endsWith("?")){
      this.questionService.createQuestion(questionData).then(() => {
        // refresh questions
        this.questionService.getListQuestions()
        // and go to home
        this.router.navigate(['/']);
      }
      )
    } else {
      console.log("erreur");
      window.alert("Le titre ne doit pas dépasser 20 mots et finir par un point d'interrogation")
   }
  }

  ngOnInit(): void {
  }
}
