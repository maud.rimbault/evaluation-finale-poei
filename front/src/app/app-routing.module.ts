import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { FormQuestionPageComponent } from './pages/form-question-page/form-question-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'questions', pathMatch: 'full'},
  { path: 'questions', component: HomePageComponent},
  { path: 'questions/form', component: FormQuestionPageComponent},
  { path: 'users/signup', component: SignupPageComponent},
  { path: 'users/login', component: LoginPageComponent},
  { path: 'questions/:questionId', component: DetailsPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
