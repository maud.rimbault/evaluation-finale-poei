import { Router } from "@angular/router";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { AuthService } from "../services/auth.service";

export const authInterceptor = (authService: AuthService): void => {
    // Add a request interceptor
    axios.interceptors.request.use(
        (config: AxiosRequestConfig) => {
            if (config.headers && config.method !== 'get') {
                //console.log('interceptor');
                //config.headers['Authorization'] = 'Basic dG90bzp0b3Rv'
                config.headers['Authorization'] = authService.getCurrentUserBasicAuthentication()
            }
            return config;
        },
        (error: AxiosError) => {
            console.error('ERROR:', error)
            Promise.reject(error)
        }
    )
};

export const errorInterceptor = (authService: AuthService, router: Router): void => {
    // Add a response interceptor
    axios.interceptors.response.use(response => {
        return response;
    }, error => {
        if (error.response.status === 401) {
            authService.logout();
            router.navigate(['/users/login']);
        }
        throw error;
    })
}

